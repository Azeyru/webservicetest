#Created by: EHLMMQI
#Date: 26/09/2018
#Main Feature file

Feature: Web service Test

Scenario Outline: Sending SOAP requests
    Given The Input XML: "<inputxml>" 
	And Sending the request "<inputxml>" to the endpoint "https://www.dataaccess.com/webservicesserver/numberconversion.wso"
    When I get the respose Compare to sample test xmls "<inputxml>"
    Then The Correct Response have been received
	
	Examples:
	|inputxml   |
	|test_1.xml |
	|test_2.xml |
	|test_3.xml |

