#Created by: EHLMMQI
#Date: 26/09/2018
#Step Definition

import os
import os.path
import sys

from behave import given, when, then

from local_files.config import xmls_to_send

@given(u'The Input XML: "{inputxml}"')
def step_1(context, inputxml):
	print(u'STEP: Given The Input XML: ' + inputxml)
	xml_data = Load_xml(context, inputxml)
	if not xml_data:
		print ("Step 1 FAILED")
	else:
		#print(xml_data)
		print('Step 1 COMPLETED')	
	
@given(u'Sending the request "{inputxml}" to the endpoint "{end_point}"')
def send_request(context, inputxml, end_point):
	print(u'STEP: Given Sending the request to the endpoint ' + end_point)
	from local_files.soap_handler import read_xml 
	read_xml(inputxml, end_point)
	print("Step 2 COMPLETED")
	
@when(u'I get the respose Compare to sample test xmls "{inputxml}"')
def check_xml(context, inputxml):
    #print(u'STEP: When I get the respose Compare to sample test xmls ' + inputxml)
	from local_files.xml_comparator_xmldiff import compare_xml
	xml = compare_xml(inputxml)
	
	if str(xml) == "['']":
		print("PASSED")
	else:
		raise NotImplementedError('Failed')
	
	print('...................'+str(xml))

@then(u'The Correct Response have been received')
def fianlly_check(context):
	
    print(u'STEP: Then The Correct Response have been received')
	#print("Run success!!!")

#Open and read the contents of the input XML
def Load_xml(context, inputxml):

	print('looking for the specified XML file in: ' + xmls_to_send)
	source_file = 'steps/local_files/'+xmls_to_send +'/' + inputxml

	try:
		with open(source_file,'r') as data:
			xml_data = data.read()
			
	except Exception as ex:
		print(ex)		
	
	return xml_data
	