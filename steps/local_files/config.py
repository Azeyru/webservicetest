#Created by: EHLMMQI
#Date: 26/09/2018
#load the folder and fixed data parameters from here

import os
import os.path

current_path = os.getcwd()

#path to xmls
xml_main_path = 'drop_xmls_here'
html_report_path = 'html_report'
xmls_to_send = xml_main_path+'\inputXML'
received_xmls = xml_main_path+'\SoapResponse'
test_xml = xml_main_path+'\TestXML'

#complete path
path_to_html_report = (os.path.join(current_path, html_report_path)).replace('\\','/')
path_to_request_xmls = (os.path.join(current_path, xmls_to_send)).replace('\\','/')
path_to_received_xmls = (os.path.join(current_path, received_xmls)).replace('\\','/')
path_to_test_xml = (os.path.join(current_path, test_xml)).replace('\\','/')

#End point
end_point = "https://www.dataaccess.com/webservicesserver/numberconversion.wso"

#Create folder if not available 
def create_folder():
	
	folder_list = [xml_main_path, html_report_path, xmls_to_send, received_xmls, test_xml]
	
	for list in folder_list:
		path = os.path.join(current_path, list)
		if not os.path.exists(path):
			os.makedirs(path)
			print ('The folder: ' + list +'has been created at ' + current_path)
	
	
if __name__ == '__main__':
	create_folder()
