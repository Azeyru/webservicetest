#Created by: EHLMMQI
#Date: 26/09/2018
#Compare xmls and create HTML report

import os
import os.path
import subprocess
import difflib

from local_files.config import current_path, received_xmls, test_xml, html_report_path

def compare_xml(xml_name):
	print("Strating XML comparator 'XMLDIFF'")
	
	response_xml = current_path + '\\steps\\local_files\\' +received_xmls + '\\' + xml_name.split(".")[0] + '_response.xml'
	test_xml_file = current_path + '\\steps\\local_files\\' + test_xml + '\\' + xml_name.split(".")[0] + '_sample.xml'
	
	try:
		cmd_xmldiff = [
					'xmldiff',
					response_xml,
					test_xml_file
					]
		process = subprocess.Popen(
							cmd_xmldiff,
							stdout = subprocess.PIPE,
							stderr = subprocess.STDOUT,
							universal_newlines=True
							)
		xml = []
		for lines in (process.stdout):
			xml.append(lines.strip())
		print(xml)
	except Exception as ex:
		print(ex)
		
	try:	
		print("Creating report")
			
		data_response_xml = open(response_xml).readlines()
		data_test_xml = open(test_xml_file).readlines()
		difference = difflib.HtmlDiff().make_file(data_response_xml, data_test_xml, 'XML1', 'XML2')	
		
		report = open (current_path + '\\steps\\local_files\\' + html_report_path + '\\' + xml_name.split(".")[0] + '.html','w')
		report.write(difference)
		report.close()
		
	except Exception as ex:
		print(ex)
	
	return xml