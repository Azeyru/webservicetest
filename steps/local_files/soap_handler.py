#Created by: EHLMMQI
#Date: 26/09/2018
#Simple SOAP handler

import os
import os.path
import requests

from local_files.config import xmls_to_send, received_xmls

def read_xml(input_xml, end_point):
	print('Opening the XML file')
	
	source_file = 'steps\\local_files\\'+xmls_to_send +'\\'+input_xml
	save_files = 'steps\\local_files\\'+received_xmls+'\\' + input_xml.split(".")[0] +'_response'+ '.xml'
	
	try:
		headers = {'content-type': 'text/xml'}
		print(source_file)
		
		with open(source_file, 'r') as xml_data:
			xml = xml_data.read()
			r = requests.post(url = end_point, data = xml, headers=headers)
			r.encoding = 'ISO-8859-1'
			
		save_response = open(save_files, 'w')
		#print (r.text)
		save_response.write(str(r.text))
		
	except Exception as ex:
		print(ex)
	
if __name__ == '__main__':
	read_xml()	
